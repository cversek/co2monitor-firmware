
#include <RHSoftwareSPI.h> // http://www.airspayce.com/mikem/arduino/RadioHead/RadioHead-1.113.zip
#include <RHRouter.h>
#include <RHMesh.h>
#include <RH_RF95.h>
#define RH_HAVE_SERIAL
#include <SPI.h>
#include <U8x8lib.h>
#include <Arduino.h>
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2
#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include "config.h"

// Change to 434.0 or other frequency, must match RX's freq!
#define RF95_FREQ 915.0
#define gatewayNode 1

SCD30 airSensor;

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 15, /* data=*/ 4, /* reset=*/ 16);

// Class to manage message delivery and receipt, using the driver declared above
RHMesh *manager;

#define BUTTON_A_PIN 37 
#define BUTTON_B_PIN 36

Button button_A = Button();
Button button_B = Button();


typedef struct {
  int co2;
  float temperature;
  float humidity;
  float light;
  char loranet_pubkey[13]; //pubkey for this lora network (same as bayou pubkey of node #1 / the gateway node)
  int node_id; 
  int next_hop;
  int next_rssi;
  int logcode;
} Payload;

Payload theData;

// heltec wifi lora 32 v2
#define RFM95_CS 18
#define RFM95_RST 14
#define RFM95_INT 26

#define LED 25

RH_RF95 rf95(RFM95_CS, RFM95_INT);

long lastMeasureTime = 0;  // the last time the output pin was toggled
long measureDelay = interval_sec*1000;

void setup() {


 button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);

    button_B.attach( BUTTON_B_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_B.interval(5); 
  button_B.setPressedState(LOW);

  
  pinMode(LED, OUTPUT);

  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);
   
  Serial.begin(115200);

  Wire.begin();

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,2); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,2); 
   u8x8.print("SCD30 detected!");
   delay(1000);
   
  //manager = new RHMesh(rf95, this_node_id);

// manual reset
  digitalWrite(RFM95_RST, LOW);
  delay(10);
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    Serial.println("Uncomment '#define SERIAL_DEBUG' in RH_RF95.cpp for detailed debug info");
    while (1);
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);
  

    // warm up CO2 sensor by waiting 2 sec
    //co2 still warming up
    u8x8.setCursor(0,6); 
    u8x8.print("Warming up...");
    delay(3000);
    u8x8.clear();
}

const __FlashStringHelper* getErrorString(uint8_t error) {
  switch(error) {
    case 1: return F("invalid length");
    break;
    case 2: return F("no route");
    break;
    case 3: return F("timeout");
    break;
    case 4: return F("no reply");
    break;
    case 5: return F("unable to deliver");
    break;
  }
  return F("unknown");
}


int firstLoop = 1;
int logcode = 1;

int co2;
float temperature;
float humidity;
float light;

// create a first random interval
int epsilon_interval = random(0,random_interval_sec*1000);


void loop() {

// on the first loop, or at a regular (but slightly randomized) interval, make a measurement
// if we're the gateway, post it to Bayou; if we're a remote node, send it to the lora network

button_A.update();

if ( button_A.pressed() ) {
  
  Serial.println("button A!");

  long pressTime = millis();
    int pressCount = 5;

    u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Keep pressing");
 u8x8.setCursor(0,2); 
 u8x8.print("to calibrate:");
 
   int buttonA_state = digitalRead(BUTTON_A_PIN);

    while (((millis() - pressTime) < 5000) && (buttonA_state==0)) {

  buttonA_state = digitalRead(BUTTON_A_PIN);
  u8x8.setCursor(0,4);
  u8x8.print(pressCount);
  u8x8.print(" ...");
  pressCount--;
  delay(1000);
  
}

if (pressCount==0) {
  
    u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Please step away");
 u8x8.setCursor(0,2);
  u8x8.print("from sensor! ");
   u8x8.setCursor(0,4);
  u8x8.print("Calibrating in:");

for (int i=30;i>0;i--) {

digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);                       // wait for a second
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
  delay(10);  
  
  u8x8.setCursor(0,6);
  u8x8.print(i);
  u8x8.print(" ...");
  delay(1000);  
}

// force calibrate
airSensor.setForcedRecalibrationFactor(forcePPM);
u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Force calibrated");
  u8x8.setCursor(0,2); 
   u8x8.print("to:");
     u8x8.setCursor(0,4); 
 u8x8.print(forcePPM);
 u8x8.print(" ppm");
 delay(3000);
 
}
else {
      u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Calibration");
 u8x8.setCursor(0,2); 
 u8x8.print("canceled.");
 delay(3000);
 firstLoop = 1;
}

   
}

// relay from mesh 
int listenTime = measureDelay+epsilon_interval;

//make a measurement if it's time

if (  ( (millis() - lastMeasureTime) > (measureDelay+epsilon_interval)) || firstLoop) {

Serial.print(millis());
Serial.print(",");
Serial.print(lastMeasureTime);
Serial.print(",");
Serial.print(measureDelay);
Serial.print(",");
Serial.print(firstLoop);
Serial.print(",");
Serial.print(measureDelay+epsilon_interval);
Serial.println();

if (airSensor.dataAvailable()) {

  co2 = airSensor.getCO2();

  if (co2 > 0) {

    // send to serial
    Serial.print("co2:");
    Serial.println(co2);

    // send to display
    u8x8.clear();

    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,2);
    u8x8.print("    "); 
    u8x8.setCursor(0,2);
    u8x8.print(co2);

    //display our node id
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(9,2);
    u8x8.print("node:");
    u8x8.print(this_node_id);

     //display our pubkey
    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    //u8x8.setCursor(0,0);
    //u8x8.print("K:");
    //u8x8.print(this_node_pubkey);

    //display our loranet_pubkey
    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    //u8x8.setCursor(0,0);
    //u8x8.print("LN:");
    //u8x8.print(loranet_pubkey);
    
    
  // make rest of measurement
  temperature = roundf(airSensor.getTemperature()* 100) / 100;
  humidity = roundf(airSensor.getHumidity()* 100) / 100;
  light = 0; // if we add a light sensor, can make this real

    sendToMesh();
 
  
  
  
  
    
} // co2 > 0
} // airSensor avail

epsilon_interval = random(0,random_interval_sec*1000); //create a new randomized interval

lastMeasureTime = millis(); // reset the interval timer 
} // measureTime



} // loop




int sendToMesh() {
  // we are a remote node, and: it's time to measure, or we're in the first loop

    firstLoop = 0; // we did our thing successfully for the first loop, so we're out of that mode
    
    Serial.println(co2);

    theData.co2 = co2;
    theData.temperature = temperature;
    theData.humidity = humidity;
    theData.light = light;
    memcpy(theData.loranet_pubkey,loranet_pubkey,13);
    theData.node_id = this_node_id;
    theData.next_rssi = rf95.lastRssi();
    theData.logcode = logcode;

    
    rf95.send((uint8_t *)&theData, sizeof(theData));
    Serial.println("Waiting for packet to complete..."); 
      delay(10);
      digitalWrite(LED, HIGH);
      rf95.waitPacketSent();
      digitalWrite(LED, LOW);
 
    
}
