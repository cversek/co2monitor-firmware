
/*
  Web client

 This sketch connects to a website (wifitest.adafruit.com/testwifi/index.html)
 using the WiFi module.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 Circuit:
 * Board with NINA module (Arduino MKR WiFi 1010, MKR VIDOR 4000 and UNO WiFi Rev.2)

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include "wiring_private.h" // pinPeripheral() function
#include <ArduinoHttpClient.h>
#include <WiFiNINA.h>
#include <ArduinoJson.h> //https://arduinojson.org/v6/doc/installation/
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>
#include <U8x8lib.h>  //U8g2 (regular not Adafruit flavor)
#include <Wire.h>
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
//#include "arduino_secrets.h"
#include <Adafruit_NeoPixel.h>

SCD30 airSensor;

#define LED_PIN    5 // for neopixels
// How many NeoPixels are attached to the Arduino?
#define LED_COUNT 8

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);


#include <RH_RF95.h>
#include <RHSoftwareSPI.h>
#include <RHRouter.h>
#include <RHMesh.h>

#define LORA_IRQ A2
#define LORA_CS A4
#define LORA_RST A5

#define LORA_SCK 13 
#define LORA_MOSI 11
#define LORA_MISO 12 

#define LORA_FREQ 915.0

U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);


// change this to match your SD shield or module;
// Arduino Ethernet shield: pin 4
// Adafruit SD shields and modules: pin 10
// Sparkfun SD shield: pin 8
// MKRZero SD: SDCARD_SS_PIN
const int chipSelect = A1;

// Configure the pins used for the ESP32 connection
#if defined(ADAFRUIT_FEATHER_M4_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0) || \
  defined(ARDUINO_AVR_FEATHER32U4) || \
  defined(ARDUINO_NRF52840_FEATHER) || \
  defined(ADAFRUIT_ITSYBITSY_M0) || \
  defined(ADAFRUIT_ITSYBITSY_M4_EXPRESS) || \
  defined(ARDUINO_AVR_ITSYBITSY32U4_3V) || \
  defined(ARDUINO_NRF52_ITSYBITSY)
  // Configure the pins used for the ESP32 connection
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    10   // Chip select pin
  #define ESP32_RESETN  7   // Reset pin
  #define SPIWIFI_ACK   9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_AVR_FEATHER328P)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     4   // Chip select pin
  #define ESP32_RESETN   3   // Reset pin
  #define SPIWIFI_ACK    2   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(TEENSYDUINO)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     5   // Chip select pin
  #define ESP32_RESETN   6   // Reset pin
  #define SPIWIFI_ACK    9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_NRF52832_FEATHER)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    16   // Chip select pin
  #define ESP32_RESETN  15   // Reset pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif !defined(SPIWIFI_SS)   // if the wifi definition isnt in the board variant
  // Don't change the names of these #define's! they match the variant ones
  #define SPIWIFI       SPI
  #define SPIWIFI_SS    10   // Chip select pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_RESETN   5   // Reset pin
  #define ESP32_GPIO0   -1   // Not connected
#endif

#define LED 13

SPIClass mySPI (&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);
// do this first, for Reasons


RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);

//RHMesh *manager;

#define gatewayNode 1


// buttons
#define BUTTON_A_PIN A3 
Bounce2::Button button_A = Bounce2::Button();

//WiFiClient wifi;
//HttpClient client = HttpClient(wifi, serverName, port);



///////please enter your sensitive data in the Secret tab/arduino_secrets.h
//char ssid[] = SECRET_SSID;        // your network SSID (name)
//char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
//int keyIndex = 0;            // your network key Index number (needed only for WEP)

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)

const char server[] = "bayou.pvos.org";    // name address for adafruit test

// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

HttpClient http = HttpClient(client, server, 80);

#if defined(EXTERNAL_FLASH_USE_QSPI)
  Adafruit_FlashTransport_QSPI flashTransport;

#elif defined(EXTERNAL_FLASH_USE_SPI)
  Adafruit_FlashTransport_SPI flashTransport(EXTERNAL_FLASH_USE_CS, EXTERNAL_FLASH_USE_SPI);

#else
  #error No QSPI/SPI flash are defined on your board variant.h !
#endif

Adafruit_SPIFlash flash(&flashTransport);

// file system object from SdFat
FatFileSystem fatfs;

const char configfile[] = "config.json";

struct Config {
  char ssid[64];
  char pswd[64];
  char pubkey[64];
  char privkey[64];
  char server[64];
  char path[64];
  int interval_sec;
  int forcePPM;
  char loranet_pubkey[13]; //pubkey for this lora network (same as bayou pubkey of node #1 / the gateway node)
  int max_wifi_attempts;
};

Config config;   

typedef struct {
  int co2;
  float temperature;
  float humidity;
  float light;
  char loranet_pubkey[13]; //pubkey for this lora network (same as bayou pubkey of node #1 / the gateway node)
  int node_id; 
  int next_hop;
  int next_rssi;
  int logcode;
} Payload;

Payload theData;

long measureDelay;

int co2;
float temperature;
float humidity;
float light;
int node_id;

void setup() {


  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)

colorWipe(strip.Color(  0, 255,   0), 50); // Green
colorWipe(strip.Color(  0, 0,   0), 50); // Black
  
  pinMode(LED, OUTPUT);
  
  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  //Serial.println("FLASH_CS:");
  //Serial.println(EXTERNAL_FLASH_USE_CS);
  
  /*while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  */

  
  digitalWrite(SPIWIFI_SS,HIGH);
  pinMode(SPIWIFI_SS,OUTPUT);
  
 button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);
  
mySPI.begin();


  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);

    u8x8.clear();
   Wire.begin();

  u8x8.setCursor(0,0); 
  if (airSensor.begin() == false)
  {
     
   u8x8.print("SCD30 missing?");
   delay(1000);
    Serial.println("Air sensor not detected. Please check wiring.");
    //while (1)
    //  ;
  } else {
    u8x8.print("SCD30 works!");
   delay(1000);

  }



   
// Assign pins 11, 12, 13 to SERCOM functionality
pinPeripheral(11, PIO_SERCOM);
pinPeripheral(12, PIO_SERCOM);
pinPeripheral(13, PIO_SERCOM);

// the pins for the virtual SPI explicitly to the internal connection
    sx1278_spi.setPins(LORA_MISO, LORA_MOSI, LORA_SCK);


    //lora ---------------------
 pinMode(LORA_RST, OUTPUT);
  digitalWrite(LORA_RST, HIGH);

  while (!rf95.init()) {
    Serial.println("LoRa radio init failed");
    Serial.println("Uncomment '#define SERIAL_DEBUG' in RH_RF95.cpp for detailed debug info");
    
       u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,2);
  u8x8.print("lora ?");
  while (1);
  
  }
  Serial.println("LoRa radio init OK!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(LORA_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
  }
  Serial.print("Set Freq to: "); Serial.println(LORA_FREQ);

  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then
  // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);

   u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,2);
  u8x8.print("lora ok!");

  

// Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1);
  }
  Serial.print("Flash chip JEDEC ID: 0x"); Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Failed to mount filesystem!");
    Serial.println("Was CircuitPython loaded on the board first to create the filesystem?");
    while(1);
  }
  Serial.println("Mounted filesystem!");

  // Check if a boot.py exists and print it out.
  if (fatfs.exists(configfile)) {
    File file = fatfs.open(configfile, FILE_READ);

StaticJsonDocument<512> doc;

DeserializationError error = deserializeJson(doc, file);
  if (error) {
    Serial.println(F("Failed to read file, using default configuration"));
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,4);
  u8x8.print("config json?");
  }
  // Copy values from the JsonDocument to the Config
  config.interval_sec = doc["interval_seconds"] | 3;
  
  strlcpy(config.ssid,                  // <- destination
          doc["wifi_ssid"] ,  // <- source
          sizeof(config.ssid));         // <- destination's capacity

  strlcpy(config.pswd,                  // <- destination
          doc["wifi_password"] ,  // <- source
          sizeof(config.pswd));         // <- destination's capacity     

             
  strlcpy(config.pubkey,                  // <- destination
          doc["public_key"] ,  // <- source
          sizeof(config.pubkey));         // <- destination's capacity 

  strlcpy(config.privkey,                  // <- destination
        doc["private_key"] ,  // <- source
        sizeof(config.privkey));         // <- destination's capacity 

  strlcpy(config.server,                  // <- destination
      doc["server"] ,  // <- source
      sizeof(config.server));         // <- destination's capacity 


  strlcpy(config.path,                  // <- destination
      doc["path"] ,  // <- source
      sizeof(config.path));         // <- destination's capacity 

      
  strlcpy(config.loranet_pubkey,                  // <- destination
      doc["loranet_pubkey"] ,  // <- source
      sizeof(config.loranet_pubkey));         // <- destination's capacity 


      config.max_wifi_attempts = doc["max_wifi_attempts"];

  config.forcePPM = doc["forcePPM"];

      
  config.interval_sec = doc["interval_seconds"];

  Serial.print("interval:");
  Serial.println(config.interval_sec);

        u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,4);
  u8x8.print("config ok!");
  
  // Close the file (Curiously, File's destructor doesn't close the file)
  //fatfs.close();

  
    /*
    Serial.println("Printing config file...");
    while (bootPy.available()) {
      char c = bootPy.read();
      //int r = atoi(c);
      //int sum = r + 3;
      Serial.print(c);
      //Serial.print("Sum:");
      //Serial.println(sum);
    }
    Serial.println();
*/


    
  }
  else {
    Serial.println("No config file found...");
      u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,4);
  u8x8.print("config file?");
  }

//Serial.println("got:");
//Serial.println(config.port);
//Serial.println(config.hostname);
    
//
//SPI.endTransaction();

 //digitalWrite(SPIWIFI_SS,LOW);
 // pinMode(SPIWIFI_SS,OUTPUT);
  
delay(1000);
  
  Serial.println("trying ...");
  // check for the WiFi module:
  WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
  
  while (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    u8x8.setCursor(0,6); 
    u8x8.print("wifi module?");
    delay(1000);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }
  Serial.print("Found firmware "); Serial.println(fv);

  u8x8.setCursor(0,6); 
    u8x8.print("wifi works!");
    delay(3000);
    
  // attempt to connect to Wifi network:
 // Serial.print("Attempting to connect to SSID: ");
 // Serial.println(config.ssid);
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:


  measureDelay=config.interval_sec*1000;


Serial.println("listening ...");
  u8x8.clear();
   u8x8.setFont(u8x8_font_7x14B_1x2_f);
  u8x8.setCursor(0,0);
  u8x8.print("Listening...");
    
}


long lastMeasureTime = 0;  // the last time the output pin was toggled


//long measureDelay = config.interval_sec*1000;
//long measureDelay = 5*1000;


int firstLoop = 1;

void loop() {

 button_A.update();
 
  if ( button_A.pressed() ) {
  
  Serial.println("button A!");

  long pressTime = millis();
    int pressCount = 5;

    u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Keep pressing");
 u8x8.setCursor(0,2); 
 u8x8.print("to calibrate:");
 
   int buttonA_state = digitalRead(BUTTON_A_PIN);

    while (((millis() - pressTime) < 5000) && (buttonA_state==0)) {

  buttonA_state = digitalRead(BUTTON_A_PIN);
  u8x8.setCursor(0,4);
  u8x8.print(pressCount);
  u8x8.print(" ...");
  pressCount--;
  delay(1000);
  
}

if (pressCount==0) {
  
    u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Please step away");
 u8x8.setCursor(0,2);
  u8x8.print("from sensor! ");
   u8x8.setCursor(0,4);
  u8x8.print("Calibrating in:");

for (int i=30;i>0;i--) {

digitalWrite(LED, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(10);                       // wait for a second
  digitalWrite(LED, LOW);    // turn the LED off by making the voltage LOW
  delay(10);  
  
  u8x8.setCursor(0,6);
  u8x8.print(i);
  u8x8.print(" ...");
  delay(1000);  
}

// force calibrate
airSensor.setForcedRecalibrationFactor(config.forcePPM);
u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Force calibrated");
  u8x8.setCursor(0,2); 
   u8x8.print("to:");
     u8x8.setCursor(0,4); 
 u8x8.print(config.forcePPM);
 u8x8.print(" ppm");
 delay(3000);
 
}
else {
      u8x8.clear();
u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,0); 
 u8x8.print("Calibration");
 u8x8.setCursor(0,2); 
 u8x8.print("canceled.");
 delay(3000);
 firstLoop = 1;
}

   
}

// end button code


// send measurement if it's past time to do so:

if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {


 if (airSensor.dataAvailable())
  {

    if (firstLoop) firstLoop = 0;

u8x8.setFont(u8x8_font_chroma48medium8_r);
  u8x8.setCursor(10,2);
  u8x8.print("...");
  
  co2 = airSensor.getCO2();
    temperature = roundf(airSensor.getTemperature()* 100) / 100;
    humidity = roundf(airSensor.getHumidity()* 100) / 100;
    node_id = 0;
    
    u8x8.clear();
    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    //u8x8.setFont(u8x8_font_inr33_3x6_f);
    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,0); 
    u8x8.print(co2);
    
    u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setCursor(0,4); 
    u8x8.print(temperature);
    u8x8.print(" C");
    u8x8.setCursor(0,6); 
    u8x8.print(humidity);
    u8x8.print(" RH");
    
    postToBayou();

  }

    lastMeasureTime = millis(); //set the current time
    
}

// check if new messages from remote lora nodes
//relayFromMesh(5000); // wait for 5000 ms for msgs from remote nodes


if (rf95.available())
  {
    // Should be a message for us now
    uint8_t buf[sizeof(Payload)];
    uint8_t len = sizeof(buf);
    
    if (rf95.recv(buf, &len))
    {
      digitalWrite(LED, HIGH);

      // the rest of this code only runs if we were the intended recipient; which means we're the gateway
      theData = *(Payload*)buf;

      
      Serial.print("node_id = ");
      Serial.print(theData.node_id);
      Serial.print("; next_hop = ");
      Serial.println(theData.next_hop);
      Serial.print("loranet:");
      Serial.println(theData.loranet_pubkey);

      co2 = theData.co2;
      temperature = theData.temperature;
      humidity = theData.humidity;
      light = theData.light;
      node_id = theData.node_id;

      neoBlink(strip.Color(  0, 255,   0), strip.Color(  0, 0,   255), node_id, 5);
        
      postToBayou();
      
      /*
      RH_RF95::printBuffer("Received: ", buf, len);
      Serial.print("Got: ");
      Serial.println((char*)buf);
       Serial.print("RSSI: ");
      Serial.println(rf95.lastRssi(), DEC);

      // Send a reply
      uint8_t data[] = "And hello back to you";
      rf95.send(data, sizeof(data));
      rf95.waitPacketSent();
      Serial.println("Sent a reply");
      digitalWrite(LED, LOW);
      */
      
    }
    else
    {
      Serial.println("Receive failed");
    }
  }

  
} // end loop


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void postToBayou() {

// Connect to wifi ...
Serial.print("Connecting to wifi ...");

  u8x8.clear();
  
u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.setCursor(0,0);
  u8x8.print("----> ");
  u8x8.print("node ");
  u8x8.print(node_id);
  u8x8.setCursor(6,2);
  u8x8.print("co2:");
  u8x8.print(co2);
  
  u8x8.setCursor(0,4);
  u8x8.print("wifi:");

  int connect_attempt = 0;
  
  
while (status != WL_CONNECTED && connect_attempt < config.max_wifi_attempts) {

    Serial.print("Attempting to connect to Network named: ");
//    Serial.println(ssid);
  Serial.println(config.ssid);
  
   
    status = WiFi.begin(config.ssid, config.pswd);

   neoBlink(strip.Color(  255, 255,   0), strip.Color(  0, 0,   0), 0, 1);

   u8x8.setCursor(0,4);
  u8x8.print("wifi:");
  u8x8.print(connect_attempt);
  u8x8.print("/");
  u8x8.print(config.max_wifi_attempts);
   connect_attempt++;
   
   
  }

if (status == WL_CONNECTED) {

  strip.setPixelColor(0, strip.Color(  255, 255,   0));         //  Set pixel's color (in RAM)
  strip.show();   
    
    u8x8.setCursor(0,4);
  u8x8.print("wifi:connected");

Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("IP Address: ");
  Serial.println(ip);


Serial.println(config.path);
Serial.println(config.pubkey);

char full_path [60];
strcpy(full_path, config.path);
strcat(full_path,config.pubkey);

Serial.println(full_path);


//Form the JSON:
  DynamicJsonDocument doc(1024);
  
//  Serial.println(post_url);

  
  doc["private_key"] = config.privkey;
  doc["co2_ppm"] =  co2;
  doc["temperature_c"]=temperature;
  doc["humidity_rh"]=humidity;
  doc["node_id"]=node_id;
  doc["log"]="OK";
   
  String json;
  serializeJson(doc, json);
  serializeJson(doc, Serial);
  Serial.println("\n");

    String contentType = "application/json";


  http.post(full_path, contentType, json);

  // read the status code and body of the response
  int statusCode = http.responseStatusCode();
  Serial.print("Status code: ");
  Serial.println(statusCode);
  String response = http.responseBody();
  Serial.print("Response: ");
  Serial.println(response);

   //u8x8.setFont(u8x8_font_chroma48medium8_r);

   //u8x8.setFont(u8x8_font_chroma48medium8_r);
   //u8x8.setFont(u8x8_font_8x13B_1x2_n);
   
  u8x8.setCursor(0,6);
  u8x8.print("http:");
  u8x8.print(statusCode);

  if(statusCode==200) {
    strip.setPixelColor(node_id, strip.Color(  0, 255,   0));         //  Set pixel's color (in RAM)
    strip.show();   
  }
}

else{
   strip.setPixelColor(0, strip.Color(  0, 0,   255));         //  Set pixel's color (in RAM)
    strip.show(); 
     u8x8.setCursor(0,4);
  u8x8.print("wifi: ??");
}
  
  //delay(config.interval_sec*1000);
 
        
}




void neoBlink(uint32_t color_1, uint32_t color_2, int node_id, int numBlinks) {
  int wait = 100; //ms
  
  for (int i=0; i<numBlinks;i++) {
    strip.setPixelColor(node_id, color_1);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
    strip.setPixelColor(node_id, color_2);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la strip.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void theaterChase(uint32_t color, int wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(int wait) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void theaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (strip.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show();                // Update strip with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  }
}

   
